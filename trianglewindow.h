﻿#ifndef TRIANGLEWINDOW_H
#define TRIANGLEWINDOW_H

#include "openglwindow.h"

#include <QtGui/QGuiApplication>
#include <QtGui/QMatrix4x4>
#include <QtGui/QOpenGLShaderProgram>
#include <QtGui/QScreen>
#include <QtCore/qmath.h>

#include <QOpenGLBuffer>
#include <QOpenGLTexture>
#include <QOpenGLExtraFunctions>

class TriangleWindow: public Openglwindow
{
public:
    TriangleWindow();
    ~TriangleWindow();

    void initialize() override;
    void render() override;

private:
    //位置
    GLuint m_posAttr;
    //颜色
    GLuint m_colAttr;
    //矩阵
    GLuint m_matrixUniform;
    //纹理
    GLuint m_texcoordLocation;
    //着色器程序
    QOpenGLShaderProgram *m_program;
    int m_frame;

    //顶点缓冲对象
    GLuint VAO;
};

#endif // TRIANGLEWINDOW_H
